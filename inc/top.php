<?php
$db = 'null';
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/style.css">
    <title>Tehtävälista</title>
  </head>
  <body>
  <div id="content">
  <?php
  try {
    $db = new PDO('mysql:host=localhost;dbname=tehtavalista;charset-utf-8','root','');
    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  }

  catch (PDOException $pdoex) {
    print "<p> Tietokannan avaus epäonnistui." .$pdoex->getMessage() . "</p>";
  }
  ?>