create database tehtavalista;
use tehtavalista;

create table tehtava (
    id int auto_increment primary key,
    kuvaus varchar(200) not null,
    tehty boolean default false,
    lisatty timestamp default current_timestamp
)